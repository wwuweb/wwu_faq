# README #

A Drupal Feature that implements an FAQ Content Type and an FAQ View, which includes a panel pane for displaying questions as well as an attachment for manually ordering the questions using DraggableViews. Some notes:

Permissions for ordering FAQ Items:

By default, all users who have the “Administer Views” permission can use the Ordering Control attachment to control the order of questions. This can be changed as needed on the “Ordering Control” display of the FAQ view under “Attachment Settings” -> Access. 

Multiple FAQ Pages:

This feature does not include the ability to have multiple FAQ pages. If this is needed, someone needs to update the FAQ Content Type and/or View to filter results by Taxonomy term(s) or OG Memberships.
