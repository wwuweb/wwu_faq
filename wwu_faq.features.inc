<?php
/**
 * @file
 * wwu_faq.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_faq_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_faq_node_info() {
  $items = array(
    'wwu_faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => t('Use the <em>FAQ</em> content type to add FAQs to your site.'),
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
